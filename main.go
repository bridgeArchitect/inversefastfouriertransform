package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"math"
	"math/cmplx"
	"os"
	"strconv"
	"strings"
)

const (
	filenameRes = "output.txt"
	N = 128
	bitsNumber = 7
	numberIteration = 7
)

func handleError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func readFile(filenameIn string, N int) []complex128 {

	var (
		file   *os.File
		err    error
		realA  []float64
		imagA  []float64
		a      []complex128
		reader *bufio.Reader
		line   []byte
		row    string
		rows   []string
		n, i   int
	)

	realA = make([]float64, N)
	imagA = make([]float64, N)
	a = make([]complex128, N)

	file, err = os.Open(filenameIn)
	handleError(err)

	reader = bufio.NewReader(file)
	n = 0
	for {

		line, _, err = reader.ReadLine()
		if err == io.EOF {
			break
		} else if err != nil {
			log.Fatal(err)
		}

		row = string(line)
		if row[0] != '#' {
			rows = strings.Split(row, " ")
			realA[n], err = strconv.ParseFloat(rows[0], 64)
			handleError(err)
			imagA[n], err = strconv.ParseFloat(rows[1], 64)
			handleError(err)
			n++
		}

	}

	err = file.Close()
	handleError(err)

	i = 0
	for i < N {
		a[i] = complex(realA[i], imagA[i])
		i++
	}

	return a

}

func readFloatNumber(msg string, number * float64) {

	var (
		err error
	)

	fmt.Println(msg)
	_, err = fmt.Scanln(number)
	handleError(err)

}

func readRow(msg string, row * string) {

	var (
		err error
	)

	fmt.Println(msg)
	_, err = fmt.Scanln(row)
	handleError(err)

}

func reverseBits(number int) int {

	var (
		invNumber int
		i         int
	)

	invNumber = 0
	for i = 0; i < bitsNumber; i++ {
		invNumber <<= 1
		invNumber |= number & 1
		number >>= 1
	}

	return invNumber

}

func makeInverseTransform(omega []complex128, number int) []float64 {

	var (
		realSign  []float64
		a         []complex128
		b         []complex128
		i, i1, i2 int
		i3, i4    int
		revIndex  int
	)

	/* create arrays */
	a = make([]complex128, number)
	b = make([]complex128, number)
	realSign = make([]float64, number)

	/* fill arrays using initial values */
	i = 0
	for i < number {
		a[i] = complex(0.0, 0.0)
		i++
	}
	i = 0
	for i < number {
		/* use inverse order of bits */
		revIndex = reverseBits(i)
		b[revIndex] = omega[i]
		i++
	}

	/* do FFT */
	i = 0
	i1 = 1
	/* go through basic iterations */
	for i < numberIteration {
		/* go through "small" element */
		i2 = 0
		for i2 < number {
			/* do calculation in one "small" element */
			i3 = 0
			for i3 < i1 {
				a[i3 + i2] = b[i3 + i2] + cmplx.Exp(complex(0.0,math.Pi * float64(i3) / float64(i1))) * b[i3 + i2 + i1]
				a[i3 + i2 + i1] = b[i3 + i2] - cmplx.Exp(complex(0.0,math.Pi * float64(i3) / float64(i1))) * b[i3 + i2 + i1]
				i3++
			}
			/* go to next "small" element */
			i2 += 2 * i1
		}
		/* increase size of "small" element */
		i1 = i1 * 2
		/* save new iteration in additional array */
		i4 = 0
		for i4 < number {
			b[i4] = a[i4]
			i4++
		}
		i++
	}

	i = 0
	for i < number {
		realSign[i] = real(a[i]) / float64(number)
		i++
	}

	return realSign

}

func writeResult(f []float64, T float64, number int) {

	var (
		fileRes *os.File
		err     error
		i       int
	)

	fileRes, err = os.Create(filenameRes)
	handleError(err)


	_, err = fileRes.WriteString("# T = " + strconv.FormatFloat(T, 'f', -1, 64) + " N = " +
		strconv.Itoa(number) + "\n")
	handleError(err)

	_, err = fileRes.WriteString("# f t\n")
	handleError(err)

	i = 0
	for i < number {
		_, err = fileRes.WriteString(strconv.FormatFloat(f[i], 'f', -1, 64) + " " +
			strconv.FormatFloat(float64(i) * T / float64(number), 'f', -1, 64) + "\n")
		handleError(err)
		i++
	}

}

func main() {

	var (
		T           float64
		filenameIn  string
		a           []complex128
		f           []float64
		//realA []float64
		//imagA []float64
	)

	readFloatNumber("Period of function:", &T)
	readRow("Name of file:", &filenameIn)

	a = readFile(filenameIn, N)
	f = makeInverseTransform(a, N)

	writeResult(f, T, N)

}